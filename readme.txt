======================================================
|		CHALLENGE 2021			     |
|		 CLASSIFIEUR			     |
|		     V1.0			     |
======================================================

Groupe :
- Simon Cavillon
- Théo Jehenne-Cousty
- Nathan Maria


Introduction :

Afin de réaliser ce classifieur, il fallait d'abord extraire le corpus des différentes pages et
au total on avait 1050 documents. Pour cela différente solutions on été utilisé pour supprimer par
exemple la ponctuation? les balises, etc ... On a également modifier un peu la fonction removeScript
de help.r qui n'était pas totalement adapté. Le classifieur utilisé pour cette version 1.0 est K-PPV
qui permet d'avoir un taux de bonification de 74-75%.

Afin d'évaluer cela, l'utilisation de la fonction erreurKPPV a été très utile permettant d'évaluer le taux d'erreur
que le classifieur peut faire concernant nos jeux de donnée. Concernant le nombre de descripteur nous avons utilisé seulement
un descripteur qui récupère les mots apparaissant 200 fois avec un nombre de K de 1 pour la fonction KPPV.

Nous avons également repris pas mal de fonction venant des derniers TP afin de les tester et les ajuster au besoin.

Installation :

Afin d'utiliser notre classifieur vous devrez en premier lieu installé les librairies suivantes :
- tm
- NLP

Afin d'y parvenir vous devrez exécuter les commandes suivantes :
- install.packages('tm',dependencies = TRUE)
- install.packages('NLP',dependencies = TRUE)

Les archives en .tar sont également disponible en cas d'erreur d'installation.


Utilisation :

Afin d'utiliser notre classifieur vous avez simplement à utiliser la fonction classer qui s'occupera de récupérer la matrice de test à partir du fichier
training.csv, après cela la fonction classer s'occupera de déterminer la classe de la page web. Si vous voulez déterminer le taux d'erreur il vous suffit de décommenter
la ligne se nommant "taux<-erreurKPPV(1, M)"

