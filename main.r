#Beaucoup d'operations sont possibles en R sur les chaines, incluant des expressions r�gulieres.
#Voir l'aide des fonctions: gsub, strsplit, grep, ...
#ou http://www.duclert.org/Aide-memoire-R/Le-langage/Chaines-de-caracteres.php

library(tm)

#----------------------------------------------------------

#SUPPRESSION DES BALISES SCRIPT
#un texte peut parfois etre considere comme un vecteur ou une liste de lignes.

#La fonction suivante concatene toutes les lignes du texte en parametre et retourne une unique chaine de caracteres

merge<-function(t){
  PlainTextDocument(paste(t,collapse=""))
}

removeScript<-function(t){
  sp<-strsplit(as.character(t), "<script")[[1]]
  vec<-sapply(1:length(sp),function(i) gsub(sp[i], pattern=".*</script>", replace=" "))
  #les �lements du split nettoy�s sont concat�n�s
  PlainTextDocument(paste(vec,collapse=""))
}

#Suppression de toutes les balises
removeBalises<-function(t){
  t1<-gsub("<[^>]*>", " ", t)
  #suppression des occurrences multiples d'espaces (ou de tabulations)
  PlainTextDocument(gsub("[ \t]+"," ",t1))
}

distance=function(x,y){
  sqrt(sum((as.integer(x)-as.integer(y))^2))
}

dist_voisins=function(vecteur, data){
  return(apply(data[,-ncol(data)], 1, distance, x=vecteur))
}

kppv<-function(vecteur,k,data){
  v<-dist_voisins(vecteur,data)
  return(order(v)[1:k])
}

classerKPPV<-function(vecteur,k,data){
  x<-kppv(vecteur,k,data)
  c<-table(data[x,ncol(data)])
  ind<-names(c)[which.max(c)]
  return(ind)
}

erreurKPPV=function(k,data){
  predict=sapply(1:nrow(data),function(i)classerKPPV(data[i,-ncol(data)],k,data[-i,]))
  classes=data[,ncol(data)]
  sum(predict!=classes)/nrow(data)
}

#Fonction de nettoyage permettant de r�cup�rer uniquement le contenu textuel d'une page HTML
nettoyage<-function(corpus){
  corpus<-tm_map(corpus,merge)
  corpus<-tm_map(corpus,content_transformer(tolower))
  #on utilise content_tranformer pour forcer le traitement via tolower à renvoyer un PlainTextDocument
  corpus<-tm_map(corpus,removeScript)
  corpus<-tm_map(corpus,removePunctuation)
  corpus<-tm_map(corpus,stripWhitespace)
  corpus<-tm_map(corpus,removeBalises)
  corpus<-tm_map(corpus,removeWords,words=stopwords('fr'))
  corpus<-tm_map(corpus,removeNumbers)
  corpus<-tm_map(corpus,stemDocument,language='fr')
}

#Exemple d'import du corpus : ATTENTION utiliser des VCorpus plut�t que des SimpleCorpus
#corpus<-VCorpus(DirSource("training",recursive=TRUE))
#corpusN<-nettoyage(corpus)
#mat<-DocumentTermMatrix(corpusN)
#vocab<-findFreqTerms(mat,200)
#matVocab<-DocumentTermMatrix(corpusN,list(dictionary=vocab))
#newMat<-as.matrix(matVocab)
#M<-cbind(newMat,c(rep("accueil",150),rep("blog",150),rep("commerce",150), rep("FAQ",150), rep("home",150), rep("liste",150), rep("recherche",150)))
#(WD <- getwd())
#if (!is.null(WD)) setwd(WD)
#write.table(M,"training.csv",sep=";",row.names=FALSE)

donnee_entrainement<-read.csv2("training.csv")

classer<-function(fic) {
  new<-Corpus(URISource(fic))
  newN<-nettoyage(new)
  mat<-DocumentTermMatrix(newN)
  vocab<-findFreqTerms(mat,200)
  newMat<-DocumentTermMatrix(newN,list(dictionary=vocab))
  res = classerKPPV(as.matrix(newMat), 2, donnee_entrainement)
  return(res)
}

taux<-erreurKPPV(1, donnee_entrainement)
#test<-classer("C:/Users/simon/OneDrive/Documents/Classificateur/classificateur/training/commerce/eShop_001.htm")

#----------------------------------------------------------

#COMPTAGE DES BALISES
#Compter par exemple le nombre de balises de 'lien' (anchor )
#une balise de 'lien' commence par "
